# XWinWrap fork

This fork adds a `class` hint to the X11 window of XWinWrap. Sample output of `xprop`:

    WM_CLASS(STRING) = "xwinwrap", "XWinWrap"

This enables window managers to detect the window and apply rules to it.

## Example: Video Wallpaper for awesome WM

The following example config will put a xwinwrap'ed MPV window on the desktop layer and enable click-through, so you can still reach the desktop menu (root menu) when you click it.
It is meant to be used in conjunction with a compositor, e.g. compton.

It requires a fairly recent MPV version and the patched xwinwrap of this repository. Tested with awesome v4.2

### Sample command using MPV

    #!/bin/bash
    xwinwrap -nf -s -st -sp -b -g 1920x1080 -fs -- mpv \
    --really-quiet --no-config --no-input-default-bindings --loop=inf --loop-file=inf --mute --vo gpu --hwdec=yes --interpolation=no --video-osd=no --osc=no --panscan=1.0 \
    -wid WID "$@"

Adjust the `-g` parameter according to your screen resolution. For multi-screen setups, you may also specify `x` and `y` coordinates to `-g` additionally to reach other screens. See `xwinwrap -h` for details.

Write this command into a file called `/usr/local/bin/mpv-wallpaper` and apply `chmod +x /usr/local/bin/mpv-wallpaper` to it.
You may then call the command and provide your video as the command line argument:

    mpv-wallpaper video.mp4

### Sample rule for awesome WM

    {
        -- this rule requires a patched version of xwinwrap that adds
        -- the corresponding window class hints
        rule = { class = "XWinWrap" },
        -- enable click-through: this allows to reach desktop menus
        callback = function(c)
            local cairo = require("lgi").cairo
            local img = cairo.ImageSurface(cairo.Format.A1, 0, 0)
            c.shape_input = img._native img:finish()
        end,
        properties = {
            floating = true,
            below = true,
            sticky = true,
            border_width = 0,
            focusable = false,
            titlebars_enabled = false
        }
    },

The above rule is just an example. YMMV depending on your specific awesome WM setup.
